# Talks

latex templates for talks and presentations

---

## Index of held talks

Links | Presentations as pdf files

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### P

[Python Tutorial](/GTQPythonTutorial_2019-04-08/latex/pythonize_yourself.pdf) | GTQ, IMB-CNM (CSIC), April 8th 2019
