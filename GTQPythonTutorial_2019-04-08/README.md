__Python Tutorial__
# Pythonize Yourself: Part II | "Python for device control"

----

The presentations's pdf file contains embedded links to code snippets contained in
an enclosed [jupyter notebook file](Scripts.ipynb). These will only be working properly, if a [jupyter notebook server](https://jupyter-notebook.readthedocs.io/en/stable/notebook.html) is running on the default port (8888) on your local machine.
