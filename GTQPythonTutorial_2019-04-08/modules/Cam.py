def get_img(n, cam, cv2):
    bgr_pic = [cam.read() for i in range(n)][-1]
    b,g,r = cv2.split(bgr_pic[1])
    rgb_pic = cv2.merge([r,g,b])
    
    return rgb_pic
    